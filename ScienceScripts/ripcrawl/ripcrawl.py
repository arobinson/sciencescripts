'''
 *******************************************************************************
 *   (c) Andrew Robinson (andrew.robinson@latrobe.edu.au) 2013                 *
 *       La Trobe University &                                                 *
 *       Life Sciences Computation Centre (LSCC, part of VLSCI)                *
 *                                                                             *
 *  This file is part of ScienceScripts.                                       *
 *                                                                             *
 *  ScienceScripts is free software: you can redistribute it and/or modify     *
 *  it under the terms of the GNU Lesser General Public License as published   *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  ScienceScripts is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU Lesser General Public License for more details.                        *
 *                                                                             *
 *  You should have received a copy of the GNU Lesser General Public License   *
 *  along with ScienceScripts.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                             *
 *******************************************************************************
 
Created on 08/02/2013

@author: arobinson
'''

import sys, getopt

from Bio import SeqIO



def main(argv):
    
    # argument defaults
    windowSize = 500
    incrementSize = 25
    headerRow = False
    maxNCount = -1
    outFunc = writeTab
    headFunc = writeTab
    collapse = False
    thresholdLow = None
    thresholdUpper = None
    
    naValue = 'N/a'
    decimalPlaces = 5
    
    formatString = "{0:." + str(decimalPlaces) + "f}"
            
    helpstr = '''ripcrawl.py [-chH] -f <format> -i <increment size> -n <max n count>
            [-t <threshold> [-C [-T <threshold>]]] -w <window size> <inputfile.fa>'''
    helpdoc = '''
 -f <str>   Output format used.  gff, tab, csv                           [default: tab]
 -i <int>   Begin RIP analysis starting at multiples of this param       [default:  25]
 -n <int>   Maximum number of N's in a window to perform analysis        [default: -1 (any)]
 -w <int>   Number of bases used in each RIP calculation, multiple of -i [default: 500]
 -t <float> Exclude windows/regions below this threshold                 [default: None]
 -T <float> Keep collapsed regions only when they peak above threshold   [default: None]
 -c         Print citation and exit
 -C         Collapse neighbouring regions into a single region
 -h         Print this help message
 -H         Print column headings (in tab and csv format)
'''
    citation = '''
  Andrew Robinson and Adam Taranto, (2013) RIPCrawl: Alignment free detection of Repeat-
  Induced Point Mutation (RIP) Signal in Fungal Genomes, Under review.

Please check back later for official publication
'''
    # parse arguments
    try:
        opts, args = getopt.getopt(argv,"cChHf:i:n:t:T:w:",[])
    except getopt.GetoptError:
        print helpstr
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-c':
            print citation
            sys.exit()
        elif opt == '-C':
            collapse = True
        elif opt == '-h':
            print helpstr
            print helpdoc
            print "Please cite the following paper if you use this tool in your research or part of a workflow:"
            print citation
            sys.exit()
        elif opt in ("-i",):
            incrementSize = int(arg)
        elif opt in ("-w",):
            windowSize = int(arg)
        elif opt == "-H":
            headerRow = True
        elif opt == "-n":
            maxNCount = int(arg)
        elif opt == "-t":
            if arg.lower() == "none":
                thresholdLow = None
            else:
                thresholdLow = float(arg)
        elif opt == "-T":
            if arg.lower() == "none":
                thresholdUpper = None
            else:
                thresholdUpper = float(arg)
        elif opt == "-f":
            arg = str(arg).lower()
            if arg == "tab":
                outFunc = writeTab
                headFunc = writeTab
            elif arg == 'gff':
                outFunc = writeGFF
                headFunc = writeNull
            elif arg == 'csv':
                outFunc = writeCSV
                headFunc = writeCSV
            else:
                print "Warning: Unknown format '%s'.  Defaulting to Tab" % arg
    
    if maxNCount < 1:
        maxNCount = int(windowSize * 0.1)
    
    if len(args) == 1:
        filename = args[0]
#        print "Processing:     '%s'" % filename
#        print "Window size:    %s" % windowSize
#        print "Increment size: %s" % incrementSize
#        print "Max N Count: %s" % maxNCount
    else:
        sys.stderr.write('Error: please specify only one file\n')
        sys.exit(3)
    
    # check windowsize and increment
    if float(windowSize)/incrementSize != windowSize/incrementSize:
        sys.stderr.write( "Error: window size must be a multiple of increment size\n")
        sys.exit(4)
    
    # check thresholding args
    if collapse and not thresholdLow:
        sys.stderr.write( "Error: you must set a threshold to collapse regions\n")
        sys.exit(5)
    if thresholdUpper and not collapse:
        collapse = True
    
    bucketCount = windowSize/incrementSize
   
    # Do the work
    handle = open(filename, "rU")
    if headerRow:
        headFunc(("SeqID",
                  "Start",
                  "End",
                  "Seq_len",
                  'N_count',
                  'Product_index',
                  'Substrate_index',
                  'Composite_RIP_index',
                  'GC_content'
                  ))
    
    # for each sequence
    for record in SeqIO.parse(handle, "fasta"):
        
        pairTotals = {'AA':0.0,'AT':0.0,'AC':0.0,'AG':0.0, 
                      'TA':0.0,'TT':0.0,'TC':0.0,'TG':0.0, 
                      'CA':0.0,'CT':0.0,'CC':0.0,'CG':0.0, 
                      'GA':0.0,'GT':0.0,'GC':0.0,'GG':0.0,}
        baseTotals = {'A':0.0, 'T':0.0, 'C':0.0, 'G':0.0,}
        incrementPairTotals = []
        incrementBaseTotals = []
        lastbase = 'N'
        baseidx = 0
        seqlen = len(record.seq)
        
        groupStart = None
        groupEnd = None
        highestRip = None
        
        # for each base in sequence
        for base in record.seq:
            
            # sum the new pair
            if base != 'N':
                baseTotals[base] += 1
                if lastbase != 'N':
                    pairTotals[lastbase+base] += 1
            
            # end of increment?
            if baseidx % incrementSize == 0 and baseidx != 0:
                
                incrementPairTotals.append(pairTotals)
                incrementBaseTotals.append(baseTotals)
                
                # ready to output a full window?
                if len(incrementPairTotals) == bucketCount:
                    
                    # sum buckets
                    windowPairTotals = reduce(lambda x, y: dict((k, v + y[k]) for k, v in x.iteritems()), incrementPairTotals)
                    windowBaseTotals = reduce(lambda x, y: dict((k, v + y[k]) for k, v in x.iteritems()), incrementBaseTotals)
                    
                    # calculate stats
                    productIndex = naValue
                    substrateIndex = naValue
                    compositeRIPIndex = naValue
                    compositeRIPIndexF = None
                    gcContent = naValue
                    
                    # ncount
                    nCount = windowSize - sum(windowPairTotals.values())
                    
                    # product index
                    if nCount <= maxNCount:
                        if windowPairTotals['AT'] > 0:
                            productIndexF = windowPairTotals['TA']/windowPairTotals['AT']
                            productIndex = formatString.format(productIndexF)
                            
                        # substrate index
                        AC_GT = windowPairTotals['AC'] + windowPairTotals['GT']
                        if AC_GT > 0:
                            substrateIndex = (windowPairTotals['CA'] + windowPairTotals['TG']) / AC_GT
                            
                            # composite rip index
                            if windowPairTotals['AT'] > 0:
                                compositeRIPIndexF = productIndexF - substrateIndex
                                compositeRIPIndex = formatString.format(compositeRIPIndexF)
                            
                            substrateIndex = formatString.format(substrateIndex)
                        
                        # GC content    
                        ACGT = windowBaseTotals['A'] + windowBaseTotals['T'] + windowBaseTotals['C'] + windowBaseTotals['G']
                        if ACGT > 0:
                            gcContent = formatString.format((windowBaseTotals['C'] + windowBaseTotals['G']) / ACGT)
                    
                    startidx = baseidx-windowSize
                    endidx = baseidx-1
                    
                    # perform collapsing
                    if collapse:
                        if compositeRIPIndexF >= thresholdLow: # going or staying above threshold?
                            if groupStart == None:
                                groupStart = startidx
                                
                            if highestRip == None or compositeRIPIndexF >= highestRip:
                                highestRip = compositeRIPIndexF
                            groupEnd = endidx
                        elif groupStart is not None and startidx > groupEnd: # 1st time beyond window size past end?
                            if highestRip >= thresholdUpper or thresholdUpper == None: # peak above threshold OR no peak threshold?
                                
                                # print the results
                                outFunc((record.id,                 # seq id
                                          str(groupStart),          # window start
                                          str(groupEnd),            # window end
                                          str(seqlen),              # sequence length
                                          str('-'),              # total N's in window
                                          str('-'),
                                          str('-'),
                                          formatString.format(highestRip),
                                          str('-'),
                                          ))
                            # reset collapse (so we don't repeat above print)
                            groupStart = None
                            groupEnd = None
                            highestRip = None
                    # perform low thresholding
                    elif thresholdLow is not None:
                        if compositeRIPIndexF >= thresholdLow:
                            # print the results
                            outFunc((record.id,                 # seq id
                                      str(startidx),            # window start
                                      str(endidx),              # window end
                                      str(seqlen),              # sequence length
                                      str(nCount),              # total N's in window
                                      str(productIndex),
                                      str(substrateIndex),
                                      str(compositeRIPIndex),
                                      str(gcContent),
                                      ))
                    # perform print all
                    else:
                        # print the results
                        outFunc((record.id,                 # seq id
                                  str(startidx),            # window start
                                  str(endidx),              # window end
                                  str(seqlen),              # sequence length
                                  str(nCount),              # total N's in window
                                  str(productIndex),
                                  str(substrateIndex),
                                  str(compositeRIPIndex),
                                  str(gcContent),
                                  ))
                        
                    # remove old bucket
                    incrementPairTotals = incrementPairTotals[1:]
                    incrementBaseTotals = incrementBaseTotals[1:]
                
                #reset counters
                pairTotals = {'AA':0.0,'AT':0.0,'AC':0.0,'AG':0.0, 
                              'TA':0.0,'TT':0.0,'TC':0.0,'TG':0.0, 
                              'CA':0.0,'CT':0.0,'CC':0.0,'CG':0.0, 
                              'GA':0.0,'GT':0.0,'GC':0.0,'GG':0.0,}
                baseTotals = {'A':0.0, 'T':0.0, 'C':0.0, 'G':0.0,}
            # update counters and trackers
            baseidx += 1
            lastbase = base
        # next base
        
        # finish off the last group if needed
        if groupStart is not None:
            if highestRip >= thresholdUpper or thresholdUpper == None:
                # print the results
                outFunc((record.id,                 # seq id
                          str(groupStart),          # window start
                          str(groupEnd),            # window end
                          str(seqlen),              # sequence length
                          str('-'),              # total N's in window
                          str('-'),
                          str('-'),
                          formatString.format(highestRip),
                          str('-'),
                          ))
    # next sequence
    handle.close()


## Output format writer functions ##
def writeTab(record):
    '''Writes a record in Tab-delimited format'''
    delimiter = "\t"
    print(delimiter.join(record))

def writeCSV(record):
    '''Writes a record in CSV format'''
    # TODO: make this correctly handle comma and quote characters
    delimiter = ","
    print(delimiter.join(record))

def writeGFF(rec):
    '''Writes a record in GFF format'''
    # TODO: implement this
    # <seqname>    <source>    <feature>      <start>    <end>    <score>    <strand>    <frame>    [attributes]
    attributes = ['NCount=%s' % rec[4],
                  'ProductIdx=%s' % rec[5],
                  'SubstrateIdx=%s' % rec[6],
                  'CompRIPIdx=%s' % rec[7],
                  'GCContent=%s' % rec[8],]
    
    
    record = [rec[0], 'RIPCrawl', 'RIP_Cluster', rec[1], rec[2], '.', '.', '.', ';'.join(attributes)]
    delimiter = "\t"
    print(delimiter.join(record))
    

def writeNull(record):
    '''A writer that doesn't produce any output.  For use with formats that 
    don't support header rows such as GFF'''
    pass


if __name__ == "__main__":
    main(sys.argv[1:])
