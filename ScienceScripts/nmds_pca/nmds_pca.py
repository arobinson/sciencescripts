'''
 ***************************************************************************
 *   (c) Andrew Robinson (andrew.robinson@latrobe.edu.au) 2013             *
 *       La Trobe University &                                             *
 *       Life Sciences Computation Centre (LSCC, part of VLSCI)            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Library General Public License (LGPL)   *
 *   as published by the Free Software Foundation; either version 2 of     *
 *   the License, or (at your option) any later version.                   *
 *                                                                         *
 *   ScienceScripts is distributed in the hope that it will be useful,     *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU Library General Public License for more details.                  *
 *                                                                         *
 *   You should have received a copy of the GNU Library General Public     *
 *   License along with ScienceScripts; if not, write to the Free Software *
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  *
 *   USA                                                                   *
 *                                                                         *
 ***************************************************************************
 
Created on 11/03/2013

@author: arobinson
'''
import sys, getopt, itertools
from Bio import SeqIO

def main(argv):
    
    # argument defaults
    fastaFilename = None
    headerRow = False
    
    
    delimiter = "\t"
    naValue = 'N/a'
    decimalPlaces = 5
    
    formatString = "{0:." + str(decimalPlaces) + "f}"
    
    
    cmdmsg = '''nmds_pca.py [-t] -f <inputfile.fa>'''
    helpmsg = '''
%s

 -h        Print this help msg
 -t        Print titles on output
 -f <str>  input FastA file
''' % cmdmsg
    
    # parse arguments
    try:
        opts, args = getopt.getopt(argv,"htf:",[])
    except getopt.GetoptError:
        print cmdmsg
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print helpmsg
            sys.exit()
        elif opt == "-t":
            headerRow = True
        elif opt == "-f":
            fastaFilename = arg
            
    if not fastaFilename:
        print "\nMissing input filename\n"
        print cmdmsg
        sys.exit(3)
        
    # populate counters
    bp = ['A','C','G','T','N'] #
    codons = [''.join(x) for x in list(itertools.product(bp,bp,bp))]
    codonCounts = {}
    for codon in codons:
        codonCounts[codon] = 0
    
    # Alternative (only populate values of interest) which is slower (~35%) than counting all values
#    grpA = ['AAT', 'ACT', 'ACG']
#    grpB = ['CAT', 'ATG', 'GGG', 'CCC']
#    grpC = ['CTC', 'ACG', 'CTG', 'CGC', 'TTA']
#    codonCounts = {}
#    for codon in grpA+grpB+grpC:
#        codonCounts[codon] = 0

    # group configuration
    groups =   [['A', 'GCT', 'GCG', 'GCC', 'GCA'],
                ['C', 'TGT', 'TGC'],
                ['D', 'GAT', 'GAC'],
                ['E', 'GAG', 'GAA'],
                ['F', 'TTT', 'TTC'],
                ['G', 'GGT', 'GGG', 'GGC', 'GGA'],
                ['H', 'CAT', 'CAC'],
                ['I', 'ATT', 'ATC', 'ATA'],
                ['K', 'AAG', 'AAA'],
                ['L', 'TTG', 'TTA', 'CTT', 'CTG', 'CTC', 'CTA'],
                ['M', 'ATG'],
                ['N', 'AAT', 'AAC'],
                ['P', 'CCT', 'CCG', 'CCC', 'CCA'],
                ['Q', 'CAG', 'CAA'],
                ['R', 'CGT', 'CGG', 'CGC', 'CGA', 'AGG', 'AGA'],
                ['S', 'TCT', 'TCG', 'TCC', 'TCA', 'AGT', 'AGC'],
                ['T', 'ACT', 'ACG', 'ACC', 'ACA'],
                ['V', 'GTT', 'GTG', 'GTC', 'GTA'],
                ['W', 'TGG'],
                ['Y', 'TAT', 'TAC'],
                ['St','TGA', 'TAG', 'TAA']]
    
    if headerRow:
        outrecord = ['SeqID']
        for group in groups:
            for codon in group[1:]:
                outrecord.append("%s-%s" % (group[0], codon))
        print delimiter.join(outrecord)
    
    faHandle = open(fastaFilename, "rU")
    for record in SeqIO.parse(faHandle, "fasta"):
        
        # fix formatting
        seq = str(record.seq.upper())
        seq = seq.replace("U", "T")
        
        # calculate codon frequency
        for codon in chunks(record.seq, 3):
            try:
                codonCounts[str(codon)] += 1
            except KeyError:
                pass
        
        # compute proportions
        outrecord = [record.id,]
        for group in groups:
            groupcount = 0
            for codon in group[1:]:
                groupcount += codonCounts[codon]
            for codon in group[1:]:
                try:
                    codonprop = formatString.format(float(codonCounts[codon])/groupcount)
                except:
                    codonprop = naValue
                outrecord.append(codonprop)

        # print results
        print delimiter.join(outrecord)
# end main()


def chunks(l, n):
    """
    Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]



if __name__ == "__main__":
    main(sys.argv[1:])
    