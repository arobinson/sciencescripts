'''
 *******************************************************************************
 *   (c) Andrew Robinson (andrew.robinson@latrobe.edu.au) 2013                 *
 *       La Trobe University &                                                 *
 *       Life Sciences Computation Centre (LSCC, part of VLSCI)                *
 *                                                                             *
 *  This file is part of ScienceScripts.                                       *
 *                                                                             *
 *  ScienceScripts is free software: you can redistribute it and/or modify     *
 *  it under the terms of the GNU Lesser General Public License as published   *
 *  by the Free Software Foundation, either version 3 of the License, or       *
 *  (at your option) any later version.                                        *
 *                                                                             *
 *  ScienceScripts is distributed in the hope that it will be useful,          *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of             *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *
 *  GNU Lesser General Public License for more details.                        *
 *                                                                             *
 *  You should have received a copy of the GNU Lesser General Public License   *
 *  along with ScienceScripts.  If not, see <http://www.gnu.org/licenses/>.    *
 *                                                                             *
 *******************************************************************************
 
Created on 28/02/2013

@author: arobinson
'''


import sys, getopt, math

from Bio import SeqIO
from BCBio import GFF
 
def main(argv):
    '''
    Performs a sliding window analysis of gene factors along an input genome 
    (scaffolds) that is tolerant of unknown bases (N's).
    
    Basic algorithm is:
    - extract required features from GFF files
    - merge features into a single stream of start/end sites (called feature 
      tags) which includes an id and their type (Gene, or N)
    - sort feature tag list
    - convert back into features and detect if Genes and N feature overlap
    - push features onto a bucketiser for Genes, N's and N's within a Gene
    - compute statistics for all the increments in each window starting at 
      every increment.
    
    A more detailed description of algorithm is as follows:
    - [Optional] scan an input fasta file that contains the scaffolds.  This
      just retrieves the sequence lengths for each scaffold.
    - Scan a GFF file for gene features.  Note: it is possible to filter the 
      GFF filter for specific feature types.  This will search for start and
      ends of gene features (called tags here).  The storage structure is:
      (<position>, Gene, <is-start?>, <auto-gen-id>).  Input genes can overlap.
    - Scan a GFF file for mask features.  Note: this can use filters and also 
      be the same file that contains Gene features.  The storage structure is:
      (<position>, N, <is-start?>, <auto-gen-id>).  The generated id's continue
      from the gene generated ids and the structure is stored in the same list
      (tagLists).  Input mask features MUST NOT overlap.
    - Compute and print statistics. For-each sequence:
      + Sort tags by starting position and process them one by one.  Store 
        starting positions in a stack and remove them when matching end is 
        found.  On ending position insert a range into bucketiser for 
        corresponding type (Gene, or N).  If its an N and there is a Gene 
        presently open then add it to the Gene and N range as well.  Note: if
        a length is available for the sequence, initialise the bucketiser with
        this value so that points after last Gene and N-less are specified 
        with 0's.
      + for each bucket (i.e. increment) store a running base count for each of
        the 3 feature type.  Also store the increment counts in a FIFO list.  
        Once the FIFO list reaches the required number of increments for a full
        window calculate/output the statistics.  Remove the oldest item from 
        the FIFO list and remove it from the running counters.  The Gene count
        is calculated by storing a running set  (of ids) and only removed from the list
        if the 2nd oldest item doesn't contain the id.
      
    Assumptions:
    - Window size is a multiple of increment.  This is critical as the 
      algorithm makes considerable optimisations based on this assumption.
    '''
    
    # argument defaults
    windowSize = 5000
    incrementSize = 500
    headerRow = False
    fastaFilename = None
    nGffFilename = None
    geneGffFilename = None
    geneFeatureNames = 'gene'
    nFeatureNames = 'region'
    maxNCount = -1
    
    # constants
    GB = 0
    NB = 1
    GNB = 2
    
#    maxNCount = -1
    
    delimiter = "\t"
    naValue = 'N/a'
    decimalPlaces = 5
    
    formatString = "{0:." + str(decimalPlaces) + "f}"
    
    cmdmsg = '''genefactor.py [-t] -w <window size> -i <increment size> -b <max bad bases> -G <gene.feature.names> 
  -N <n.feature.names> -f <inputfile.fa> -g <gene.gff> -n <Ns.gff>'''
    helpmsg = '''
%s

 -h        Print this help msg
 -t        Print titles on output
 -w <int>  Window size (default: 5000)
 -i <int>  Increment size (default: 500)
 -b <int>  The maximum number of bad bases in a window to accept, (default: 10%% of window size)
 -G <str>  Gene feature names, a comma separated list (no spaces), (default: gene)
 -N <str>  N feature names, a comma separated list (no spaces), (default: region)
 -f <str>  Optional fasta file of sequences in gff files. Used to calculate the
           length of the sequences for output
 -g <str>  GFF file containing gene information.
 -n <str>  GFF file containing the N blocks information.
''' % cmdmsg
    
    # parse arguments
    try:
        opts, args = getopt.getopt(argv,"htw:i:G:N:f:g:n:b:",[])
    except getopt.GetoptError:
        print cmdmsg
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print helpmsg
            sys.exit()
        elif opt in ("-w",):
            windowSize = int(arg)
        elif opt in ("-i",):
            incrementSize = int(arg)
        elif opt == "-t":
            headerRow = True
        elif opt == "-G":
            geneFeatureNames = arg
        elif opt == "-N":
            nFeatureNames = arg
        elif opt == "-f":
            fastaFilename = arg
        elif opt == "-g":
            geneGffFilename = arg
        elif opt == "-n":
            nGffFilename = arg
        elif opt == "-b":
            maxNCount = int(arg)
    
    # compute extra options
    geneFeatureNamesList = geneFeatureNames.split(',')
    nFeatureNamesList = nFeatureNames.split(',')
    bucketCount = windowSize/incrementSize
    if maxNCount < 1:
        maxNCount = int(windowSize * 0.1)
    
    if int(windowSize)/int(incrementSize) != float(windowSize)/float(incrementSize):
        sys.stderr.write("Error: Window size must be a multiple of increment size!\n")
        sys.exit(1)
        
    
    ### (1) calculate lengths from fastafile (if provided) ###
    seqLengths = {}
    if fastaFilename:
        faHandle = open(fastaFilename, "rU")
        for record in SeqIO.parse(faHandle, "fasta"):
            seqLengths[record.id] = len(record.seq)
    
    ### (2) parse the gene gff file ###
    limitInfo = {'gff_type': geneFeatureNamesList}
    geneHandle = open(geneGffFilename)
    geneLists = []
    tagLists = {}
    featid = 1
    for sequence in GFF.parse(geneHandle, limit_info=limitInfo):
        
        # make a list of gene locations
        # Note: this performs gene consolidation via a delayed appending loop
        geneList = []
        tagList = []
        lastGene = None
        for feature in sequence.features:
            gene = (int(feature.location.start), int(feature.location.end))
            
            # delayed processing to check for overlaps
            if lastGene: # true on 2+ iteration
                if lastGene[1] >= gene[0]: # overlap?
                    lastGene = (lastGene[0], gene[1])
                else:   
                    geneList.append(lastGene)
                    tagList.append((lastGene[0], GB, True, featid))
                    tagList.append((lastGene[1], GB, False, featid))
                    lastGene = gene
            else:
                lastGene = gene    
            featid += 1
        # ^ next feature ^
        if lastGene:
            geneList.append(lastGene)
            tagList.append((lastGene[0], GB, True, featid))
            tagList.append((lastGene[1], GB, False, featid))
        
        geneLists.append((sequence.id, geneList))
        tagLists[sequence.id] = tagList
    # ^ next sequence ^
    geneHandle.close()
    
    ### (3) parse the Ns gff file ###
    limitInfo = {'gff_type': nFeatureNamesList}
    nHandle = open(nGffFilename)
    nLists = {}
    for sequence in GFF.parse(nHandle, limit_info=limitInfo):
        
        # load taglist for this sequence from the gene step
        try:
            tagList = tagLists[sequence.id]
        except: 
            # create a new one if it didn't exist
            tagList = []
        nList = []
        for feature in sequence.features:
            npiece = (int(feature.location.start), int(feature.location.end))
            nList.append(npiece)
            tagList.append((npiece[0], NB, True, featid))
            tagList.append((npiece[1], NB, False, featid))
            featid += 1
        tagLists[sequence.id] = tagList
        nLists[sequence.id] = nList
    nHandle.close()
    
    if headerRow:
        print delimiter.join(["SeqID",
                                "Start",
                                "End",
                                "GeneBases",
                                "NBases",
                                "GeneNBases",
                                "GeneCount",
                                "GeneFactor",
                               ])
        
    ### (4) calculate/print the statistics ###
    for seqid, tagList in tagLists.items():
        activeTags = [[],[]]
        
        # get the sequence length if we scanned the fasta file
        try:
            seqlen = seqLengths[seqid]
        except:
            seqlen = 0

        # sort the tags so that they can be processed
        tagList.sort()

        ## (4a) foreach tag add ranges to the bucketiser ##
        statBuckets = SizeCountBucketiser(incrementSize, 3, seqlen)
        for tag in tagList:
            
            if tag[1] == GB and len(activeTags[NB]) > 0:
                sys.stderr.write("Warning: start or end of gene in N section!\n")
            
            if tag[2]: # start tag ?
                
                activeTags[tag[1]].append(tag[0])
            else:
                tagStart = activeTags[tag[1]].pop()
                
                # make standard range counts
                if len(activeTags[tag[1]]) == 0: # if closing last concurrent tag
                    statBuckets.insertRange(tag[1], tagStart, tag[0], tag[3])
                
                if tag[1] == NB and len(activeTags[GB]) > 0: # closing NBlock within GeneBlock (INFO: assumes NBlock cannot span GeneBlock boundary)
                    statBuckets.insertRange(GNB, tagStart, tag[0], None)
            # endif
        # ^ next tag ^
        
        ## (4b) calculate/print the statistics ## 
        buckets = statBuckets.buckets
        startbase = 1
        endbase = windowSize
        windowBucketList = []
        geneBaseCount = 0
        nBaseCount = 0
        geneNBaseCount= 0
        windowIDSet = set([])
        for i in range(len(buckets[GB])): # for each increment
                
            windowBucketList.append((buckets[GB][i][0], buckets[NB][i][0], buckets[GNB][i][0], buckets[GB][i][2]))
            
            geneBaseCount += buckets[GB][i][0]
            nBaseCount += buckets[NB][i][0]
            geneNBaseCount += buckets[GNB][i][0]
            windowIDSet.update(buckets[GB][i][2])
            
            if len(windowBucketList) >= bucketCount: # reached the required windowsize?
                
                # main statistic
                genefactor = naValue
                if (windowSize - nBaseCount) != 0 and nBaseCount <= maxNCount:
                    genefactor = formatString.format((float(geneBaseCount) - geneNBaseCount) / (float(windowSize) - nBaseCount) * len(windowIDSet))
                    
                # print output record
                print delimiter.join([
                                      seqid,
                                      str(startbase),
                                      str(endbase),
                                      str(geneBaseCount),
                                      str(nBaseCount),
                                      str(geneNBaseCount),
                                      str(len(windowIDSet)),
                                      genefactor,
                                      ])
            
                # remove the old bucket from the list and counters
                oldBucket = windowBucketList.pop(0)
                geneBaseCount -= oldBucket[0]
                nBaseCount -= oldBucket[1]
                geneNBaseCount -= oldBucket[2]
                for featid in oldBucket[3]:
                    if featid not in windowBucketList[0][3]: # not in next-last window
                        windowIDSet.remove(featid)
                
                startbase += incrementSize
                endbase += incrementSize
        pass
    

class SizeCountBucketiser(object):
    '''An object that calculates the size and count of segments that are contained in buckets'''
    
    def __init__(self, bucketSize, groups, initialSize = 0):
        '''
        Create a new Size and Count Bucketiser
        @param bucketSize: the size of each bucket
        @param groups: the number of separate groups to calculate on    
        '''
        self.bucketSize = bucketSize
        self.groups = groups
        if initialSize > 0:
            # self.buckets[<group>][<bucket>][<size>,<count>,set([<featid>])]
            self.buckets = [[[0,0,set([])] for j in range(initialSize/bucketSize)] for i in range(groups)]
        else:
            self.buckets = [[] for i in range(groups)]
            
    def insertRange(self, group, start, end, fid):
        '''
        Inserts a range element into the selected group.
        @param group: the group id to add too, 0-index and less than groups
        @param start: the start position, 1-index, inclusive and <= end
        @param end: the end position, 1-index, inclusive and >= start
        @param fid: the feature of this range
        '''
        
        if group <= self.groups and group >= 0:
            bucketList = self.buckets[group]
            
            bucketStart = (start - 1) / self.bucketSize
            bucketEnd = (end - 1) / self.bucketSize
            
            # make sure the list is long enough
            curlen = len(bucketList)
            if bucketEnd >= curlen:
                for bid in range(self.groups):
                    self.buckets[bid].extend([[0,0,set([])] for j in range(bucketEnd + 1 - curlen)])
                pass
            
            # add end pieces
            if bucketStart < bucketEnd: # piece crosses bucket boundary
                bucketList[bucketStart][0] += (bucketStart + 1) * self.bucketSize - start + 1
                bucketList[bucketEnd][0] += end - bucketEnd * self.bucketSize
                bucketList[bucketStart][1] += 1
                bucketList[bucketStart][2].add(fid)
                bucketList[bucketEnd][1] += 1
                bucketList[bucketEnd][2].add(fid)
            else:
                bucketList[bucketEnd][0] += end - start + 1
                bucketList[bucketEnd][1] += 1
                bucketList[bucketEnd][2].add(fid)
            
            # add any completely overlapped buckets
            for bucketPos in range(bucketStart + 1, bucketEnd):
#                print "full span"
                bucketList[bucketPos][0] += self.bucketSize
                bucketList[bucketPos][1] += 1
                bucketList[bucketPos][2].add(fid)
                
#        self.output()

    def output(self):
        print self.buckets


if __name__ == "__main__":
    main(sys.argv[1:])


